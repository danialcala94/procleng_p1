package regularexpresions;

import java.util.regex.Pattern;

public class Comparator {
	
	private static boolean validate(String text, String regExp) {
		return Pattern.matches(regExp, text);
	}
	
	/**
	 * Tests if provided text has a valid Spanish DNI format.
	 * @param text
	 * @return True if yes, false if not.
	 */
	public static boolean isDNI(String text) {
		return validate(text, RegularExpressions.DNI);
	}
	
	/**
	 * Tests if provided text has a valid Spanish NIE format.
	 * @param text
	 * @return True if yes, false if not.
	 */
	public static boolean isNIE(String text) {
		return validate(text, RegularExpressions.NIE);
	}
	
	/**
	 * Tests if provided text is a real number with (or without) exponent.
	 * @param text
	 * @return True if yes, false if not.
	 */
	public static boolean isRealNumberWithExponent(String text) {
		return validate(text, RegularExpressions.REAL_NUMBER_WITH_EXPONENT);
	}
	
	/**
	 * Tests if provided text has a valid web page domain format.
	 * @param text
	 * @return True if yes, false if not.
	 */
	public static boolean isDomain(String text) {
		return validate(text, RegularExpressions.DOMAIN);
	}
	
	/**
	 * Tests if provided text has a valid email format.
	 * @param text
	 * @return True if yes, false if not.
	 */
	public static boolean isEmail(String text) {
		return validate(text, RegularExpressions.EMAIL);
	}
	
	/**
	 * Tests if provided text is a function name declaration.
	 * @param text
	 * @return True if yes, false if not.
	 */
	public static boolean isFunctionName(String text) {
		return validate(text, RegularExpressions.FUNCTION_NAME);
	}

}
